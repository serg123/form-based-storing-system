<?php
include('HTMLGenerator.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit existing</title>
    <script>
        function onCancelClick(template) {
            window.location.href = encodeURI("index.php?template=" + template);
        }
    </script>
    <style>
        select, input:not([type="submit"]):not([type="checkbox"]) {
            width: 100%;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<form method="post">
    <div style="display:inline-block">
        <?php
        $htmlGenerator = new HTMLGenerator($templateManager);
        $formHTML = $htmlGenerator->generateFilledForm($templateFormData);
        echo $formHTML;
        ?>
        <div style="display:flex; justify-content:space-between; padding-top:12px">
            <button type="button" onclick="onCancelClick('<?php echo $templateFormData["template"] ?>')">Cancel</button>
            <?php
            if ($formHTML)
                echo "<input type=\"submit\" value=\"Save\">";
            ?>
        </div>
    </div>
</form>
</body>
</html>