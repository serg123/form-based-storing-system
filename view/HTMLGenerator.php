<?php

class HTMLGenerator{

    private $templateManager;

    public function __construct($templateManager){
        $this->templateManager = $templateManager;
    }

    public function generateForm($templateName){
        $template = $this->templateManager->getTemplate($templateName);
        if (!$template)
            return null;
        $formFields = $template->form;
        $html = "<table>";
        foreach ($formFields as $key => $value) {
            switch ($value->type) {
                case "string":
                    $html .= $this->stringInput($key, "", $value->min, $value->max);
                    break;
                case "integer":
                    $html .= $this->integerInput($key, 0, $value->min, $value->max);
                    break;
                case "boolean":
                    $html .= $this->booleanInput($key, false);
                    break;
                case "enumeration":
                    $html .= $this->enumerationInput($key, "", $value->values);
                    break;
                case "date":
                    $html .= $this->dateInput($key, "");
                    break;
            }
        }
        $html .= "</table><input type=\"hidden\" name=\"template\" value=\"{$templateName}\">";
        return $html;
    }

    public function generateFilledForm($templateFormData){
        $template = $this->templateManager->getTemplate($templateFormData["template"]);
        if (!$template)
            return null;
        $formFields = $template->form;
        $formData = $templateFormData["data"];
        $html = "<table>";
        foreach ($formFields as $key => $value) {
            $fieldValue = control($formData->$key);
            switch ($value->type) {
                case "string":
                    $html .= $this->stringInput($key, $fieldValue, $value->min, $value->max);
                    break;
                case "integer":
                    $html .= $this->integerInput($key, $fieldValue, $value->min, $value->max);
                    break;
                case "boolean":
                    $html .= $this->booleanInput($key, $fieldValue);
                    break;
                case "enumeration":
                    $html .= $this->enumerationInput($key, $fieldValue, $value->values);
                    break;
                case "date":
                    $html .= $this->dateInput($key, $fieldValue);
                    break;
            }
        }
        $html .= "</table><input type=\"hidden\" name=\"id\" value=\"{$templateFormData["id"]}\">";
        return $html;
    }

    private function stringInput($name, $value, &$min, &$max){
        $valueHTML = "value=\"{$value}\"";
        $minHTML = "";
        if (isset($min))
            $minHTML = "minlength=\"{$min}\"";
        $maxHTML = "";
        if (isset($max))
            $maxHTML = "maxlength=\"{$max}\"";
        return "<tr><td>{$name}: </td><td><input {$minHTML} {$maxHTML} {$valueHTML} type=\"text\" name=\"{$name}\"></input></td></tr>";
    }

    private function integerInput($name, $value, &$min, &$max){
        if ($value < $min)
            $value = $min;
        else if ($value > $max)
            $value = $max;
        $valueHTML = "value=\"{$value}\"";
        $minHTML = "";
        if (isset($min))
            $minHTML = "min=\"{$min}\"";
        $maxHTML = "";
        if (isset($max))
            $maxHTML = "max=\"{$max}\"";
        return "<tr><td>{$name}: </td><td><input {$minHTML} {$maxHTML} {$valueHTML} type=\"number\" name=\"{$name}\"></input></td></tr>";
    }

    private function booleanInput($name, $value){
        $valueHTML = $value ? "checked" : "";
        return "<tr><td>{$name}: </td><td><input {$valueHTML} type=\"checkbox\" name=\"{$name}\"></input></td></tr>";
    }

    private function enumerationInput($name, $value, $values){
        $options = "";
        foreach ($values as $val) {
            if ($value == $val)
                $options .= "<option selected value=\"{$val}\">{$val}</option>";
            else
                $options .= "<option value=\"{$val}\">{$val}</option>";
        }
        return "<tr><td>{$name}: </td><td><select name=\"{$name}\">{$options}</input></td></tr>";
    }

    private function dateInput($name, $value){
        $valueHTML = "value=\"{$value}\"";
        return "<tr><td>{$name}: </td><td><input {$valueHTML} type=\"date\" name=\"{$name}\"></input></td></tr>";
    }

    public function generateTable($templateName, $tableData){
        $template = $this->templateManager->getTemplate($templateName);
        if (!$template)
            return null;
        $columns = $template->table;
        $html = "<table id=\"table\">";
        $html .= $this->tableHeader($columns);
        foreach ($tableData as $rowData) {
            $html .= "<tr>";
            foreach ($columns as $key => $value)
                $html .= $this->tableCell($value, $rowData["data"], $template->form);
            $html .= $this->tableButtons($templateName, $rowData["id"]);
            $html .= "</tr>";
        }
        $html .= "</table>";
        return $html;
    }

    private function tableHeader($columns){
        $html = "<tr>";
        foreach ($columns as $key => $value)
            $html .= "<th onclick=\"onHeaderClick('{$key}')\">{$key}</th>";
        $html .= "<th></th><th></th></tr>";
        return $html;
    }

    private function tableCell($cellPattern, $rowData, $formFields){
        $html = "<td>";
        preg_match_all('/\$\{.+?\}/', $cellPattern, $matches);
        $result = $cellPattern;
        foreach ($matches[0] as $match) {
            $key = substr($match, 2, strlen($match) - 3);
            if (!isset($formFields->$key))
                continue;
            if ($formFields->$key->type == "boolean")
                $replacement = control($rowData->$key) ? "<input type=\"checkbox\" checked disabled>" : "<input type=\"checkbox\" disabled>";
            else if ($formFields->$key->type == "date") {
                if (control($rowData->$key)) {
                    try {
                        $replacement = (new DateTime(control($rowData->$key)))->format("d.m.Y");
                    } catch (Exception $e) {
                        $replacement = (new DateTime())->format("d.m.Y");
                    }
                } else
                    $replacement = "[not specified]";
            } else
                $replacement = control($rowData->$key);
            $result = str_replace($match, $replacement, $result);
        }
        $html .= $result;
        $html .= "</td>";
        return $html;
    }

    private function tableButtons($templateName, $id){
        $html = "<td><button onclick=\"onEditClick({$id})\">Edit</button></td>";
        $html .= "<td><button onclick=\"onDeleteClick({$id})\">Delete</button></td>";
        return $html;
    }

}