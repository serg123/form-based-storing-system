<?php
include('HTMLGenerator.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>List all</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script>
        function onSelectChange(event) {
            var selectedTemplate = event.target.value;
            window.location.href = encodeURI("index.php?template=" + selectedTemplate);
        }
        function onAddClick() {
            var selectedTemplate = document.getElementById("template").value;
            if (selectedTemplate)
                window.location.href = encodeURI("edit.php?template=" + selectedTemplate);
        }
        function onEditClick(id) {
            window.location.href = encodeURI("edit.php?id=" + id);
        }
        function onDeleteClick(id) {
            window.location.href = encodeURI("edit.php?delete=" + id);
        }
        function onHeaderClick(columnName) {     //sort rows alphabetically ascending and update the table
            var columnIndex = tableCols.indexOf(columnName);
            tableRows.sort(function (a, b) {
                a = a.data[columnIndex].toLowerCase();
                b = b.data[columnIndex].toLowerCase();
                aDate = moment(a, "DD.MM.YYYY");
                bDate = moment(b, "DD.MM.YYYY");
                if (aDate._isValid && bDate._isValid)
                    return aDate._d - bDate._d;
                else
                    return a >= b ? 1 : -1;
            });
            var table = document.getElementById("table");
            var html = "<tr>" + table.rows[0].innerHTML + "</tr>";
            for (var i = 0; i < tableRows.length; i++)
                html += ("<tr>" + tableRows[i].html + "</tr>");
            table.innerHTML = html;
            for (var i = 0; i < table.rows[0].cells.length - 2; i++)
                table.rows[0].cells[i].classList.remove("sort");
            table.rows[0].cells[columnIndex].classList.add("sort");
        }
        onload = function () {     //for client-side sort, extract data from the server-generated table
            var table = document.getElementById("table");
            tableCols = [];
            tableRows = [];
            for (var i = 0; i < table.rows[0].cells.length - 2; i++)
                tableCols.push(table.rows[0].cells[i].innerHTML);
            for (var i = 1; i < table.rows.length; i++) {
                var data = [];
                for (var j = 0; j < table.rows[i].cells.length - 2; j++)
                    data.push(table.rows[i].cells[j].innerHTML);
                tableRows.push({
                    html: table.rows[i].innerHTML,
                    data: data
                });
            }
        }
    </script>
    <style>
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
            padding: 6px;
        }
        th {
            background: lightgrey;
            cursor: pointer;
        }
        th.sort {
            cursor: default;
        }
        th.sort::after {
            content: "\25B2"
        }
    </style>
</head>
<body>
<div>
    Template:
    <select onchange="onSelectChange(event)" id="template">
        <?php
        foreach ($templateNames as $templateName) {
            $selected = ($selectedTemplateName == $templateName ? "selected" : "");
            echo "<option {$selected} value=\"{$templateName}\">{$templateName}</option>";
        }
        ?>
    </select>
</div>
<div style="display:inline-block; padding-top:12px">
    <?php
    $htmlGenerator = new HTMLGenerator($templateManager);
    $tableHTML = $htmlGenerator->generateTable($selectedTemplateName, $templateFormData);
    echo $tableHTML;
    ?>
    <div style="display:flex; flex-direction:row-reverse; padding-top:12px">
        <?php if ($tableHTML)
            echo "<button onclick=\"onAddClick()\">Add</button>";
        ?>
    </div>
</div>
</body>
</html>