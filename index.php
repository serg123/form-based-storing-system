<?php
include("helpers/methods.php");
include("model/TemplateManager.php");
include("model/DAO.php");

try {
    $templateManager = new TemplateManager();
    $templateNames = $templateManager->getTemplateNames();
    $dao = new DAO();
    if (isset($_GET["template"]))
        $selectedTemplateName = urldecode($_GET["template"]);
    else if (sizeof($templateNames) > 0)
        $selectedTemplateName = $templateNames[0];
    else
        $selectedTemplateName = null;
    $templateFormData = $dao->getByTemplate($selectedTemplateName);
    render("index", ["templateNames" => $templateNames, "selectedTemplateName" => $selectedTemplateName, "templateFormData" => $templateFormData, "templateManager" => $templateManager]);
} catch (Exception $e) {
    render("error", ["message" => $e->getMessage()]);
}