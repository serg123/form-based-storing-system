# Form-based data storing system

### Objectives 
The main goal of the test task is to find out if and how applicant is capable of designing and implementing 
a simple working system according to given specification.   
The system is a web based application meant for performing common tasks on various not hard-coded 
data forms, such as adding, editing, removing a form and viewing a form data. 

### System definition 
Forms  are  described  in  editable  templates  that  contain  form  name,  types  of  fields  with  possible 
restrictions, and other necessary information. 
Each form template used in the system must be described in separate XML or JSON file. 
System must allow user to add a new form templates and delete form templates which are in use. 
Adding a form template can be performed by uploading an XML or JSON file to the server (file upload 
using web-forms). Though it is not necessary to implement this, it’s enough if system can just read XML 
or JSON files from some folder. 
If XML or JSON contains any errors and can't be parsed, it must be reported to user. 
Data entered to form must be stored in database. 

### Form definition 
Data form may contain any set of fields of the following types:

- Text (restrictions such as min and max length may be applied) 
- Number (restrictions such as min and max value may be applied) 
- Set of predefined values (one value may be chosen) e.g. “Allow”, “Deny” or “Mon”, “Tue, 
“Wed”, ... 
In HTML form this type must be represented as a drop-down list. 
- Boolean (check box) 
- Date (Format: DD.MM.YYYY)

Each field in form must have a title. 
In case if user enters data in wrong format, in must be reported to him.

### System front page 
System front page must contain the following controls:

- Drop-down list for selecting a form template to work with. On selecting some item from drop-
down a table with entered data must be shown. This list must contain human readable form names, 
not just XML or JSON file names.
ARGM4191 
- Table with entered data. Table columns must reflect form fields (with some exceptions, see 
below). There must be “Edit record” and “Delete record” button in each table row. 
- “Add” button for adding a new record.

Data in table may be a simple combination of entered fields.
I.e. the format of output data must not be hardcoded but must be configurable in form template. Even 
after the data is collected user must be able to change the format of columns of data table by making 
modification of a template. 
User must have ability to sort a table by any of a column. 

### Task output 
- Code: Uses pure php 5 + MySQL without any libraries or frameworks. (Only uses moment.js for date string parsing client-side)
- DDF: db.sql in the root folder
- JSON templates: examples are in the templates folder