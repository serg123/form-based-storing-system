<?php

class DAO{

    const HOST = "localhost";
    const USER = "form_based_storing_system_user";
    const PASSWORD = "form_based_storing_system_password";
    const DB = "form_based_storing_system";

    private $connection;

    public function __construct(){
        $this->connection = new mysqli(self::HOST, self::USER, self::PASSWORD, self::DB);
    }

    public function getByTemplate($templateName){
        $sql = "SELECT * FROM data_storage WHERE template='{$templateName}'";
        $query = $this->connection->query($sql);
        $result = [];
        while ($row = $query->fetch_assoc())
            $result[] = ["data" => json_decode($row["data"]), "id" => $row["id"], "template" => $row["template"]];
        return $result;
    }

    public function getById($id){
        $sql = "SELECT * FROM data_storage WHERE id='{$id}'";
        $query = $this->connection->query($sql);
        while ($row = $query->fetch_assoc())
            return ["data" => json_decode($row["data"]), "id" => $row["id"], "template" => $row["template"]];
    }

    public function deleteById($id){
        $sql = "DELETE FROM data_storage WHERE id={$id}";
        $this->connection->query($sql);
    }

    public function create($template, $data){
        $json = json_encode($data);
        $sql = "INSERT INTO data_storage(template, data) VALUES('{$template}','{$json}')";
        $this->connection->query($sql);
    }

    public function update($id, $data){
        $json = json_encode($data);
        $sql = "UPDATE data_storage SET data='{$json}' WHERE id={$id}";
        $this->connection->query($sql);
    }

}