<?php

class ArgumentParser{

    private $templateManager;

    public function __construct($templateManager){
        $this->templateManager = $templateManager;
    }

    public function parse($templateName, $arguments){
        $template = $this->templateManager->getTemplate($templateName);
        $formFields = $template->form;
        $result = [];
        foreach ($formFields as $key => $value) {
            $argumentsKey = str_replace(" ", "_", $key);
            $argument = control($arguments[$argumentsKey]);
            switch ($value->type) {
                case "boolean":
                    $result[$key] = isset($arguments[$argumentsKey]) ? true : false;
                    break;
                case "integer":
                    if (is_numeric($argument)) {
                        $this->templateConstraintsControl($key, $argument, $template->form->$key);
                        $result[$key] = $argument;
                    } else
                        throw new Exception ("\"{$key}\" is not valid integer");
                    break;
                case "string":
                    $this->templateConstraintsControl($key, $argument, $template->form->$key);
                    $result[$key] = $argument;
                    break;
                case "date":
                    try {
                        new DateTime($argument);
                        $result[$key] = $argument;
                    } catch (Exception $e) {
                        throw new Exception ("\"{$key}\" is not valid date");
                    }
                    break;
                case "enumeration":
                    $this->templateConstraintsControl($key, $argument, $template->form->$key);
                    $result[$key] = $argument;
                    break;
                default:
                    throw new Exception("unsupported template");
            }
        }
        return $result;
    }

    private function templateConstraintsControl($key, $value, $constraints){
        if ($constraints->type == "integer") {
            $value = (int)$value;
            if (isset($constraints->min) && $value < $constraints->min)
                throw new Exception ("\"{$key}\" is too small");
            if (isset($constraints->max) && $value > $constraints->max)
                throw new Exception ("\"{$key}\" is too big");
        }
        if ($constraints->type == "string") {
            if (isset($constraints->min) && strlen($value) < $constraints->min)
                throw new Exception ("\"{$key}\" is too short");
            if (isset($constraints->max) && strlen($value) > $constraints->max)
                throw new Exception ("\"{$key}\" is too long");
        }
        if ($constraints->type == "enumeration" && isset($constraints->values)) {
            if (!in_array($value, $constraints->values))
                throw new Exception ("\"{$key}\" is not valid enumeration value");
        }
    }
}