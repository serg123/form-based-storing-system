<?php

class TemplateManager{

    const templateFolder = "templates";
    private $templates = [];

    public function __construct(){
        $templateTextArray = $this->readTemplates();
        foreach ($templateTextArray as $templateText) {
            $template = json_decode($templateText);
            if ($template) {
                $this->controlTemplate($template, $templateText);
                $this->templates[] = $template;
            } else
                throw new Exception("template definition is not in valid json format: <pre>{$templateText}</pre>");
        }
    }

    public function getTemplateNames(){
        $names = [];
        foreach ($this->templates as $template)
            $names[] = $template->name;
        return $names;
    }

    public function getTemplate($templateName){
        foreach ($this->templates as $template) {
            if ($template->name == $templateName)
                return $template;
        }
    }

    private function readTemplates(){
        $templateTextArray = [];
        $dir = new DirectoryIterator(self::templateFolder);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot())
                $templateTextArray[] = file_get_contents(self::templateFolder . "/" . $fileinfo->getFilename());
        }
        return $templateTextArray;
    }

    private function controlTemplate($template, $templateText){
        if (!isset($template->name))
            throw new Exception("template has no name property: <pre>{$templateText}</pre>");
        if (!is_string($template->name))
            throw new Exception("template name has to be a string: <pre>{$templateText}</pre>");

        if (!isset($template->table))
            throw new Exception("template has no table property: <pre>{$templateText}</pre>");
        if (!is_object($template->table))
            throw new Exception("template table has to be an object: <pre>{$templateText}</pre>");
        foreach ($template->table as $key => $value)
            if (!is_string($value))
                throw new Exception("table \"{$key}\" property has to be a string: <pre>{$templateText}</pre>");

        if (!isset($template->form))
            throw new Exception("template has no form property: <pre>{$templateText}</pre>");
        if (!is_object($template->form))
            throw new Exception("template form has to be an object: <pre>{$templateText}</pre>");
        foreach ($template->form as $key => $value) {
            if (!is_object($value))
                throw new Exception("form \"{$key}\" property has to be an object: <pre>{$templateText}</pre>");
            if (!isset($value->type))
                throw new Exception("\"{$key}\" has no type property: <pre>{$templateText}</pre>");
            if (!($value->type == "string" || $value->type == "integer" || $value->type == "date" || $value->type == "enumeration" || $value->type == "boolean"))
                throw new Exception("\"{$key}\" type  has to be \"string\", \"integer\", \"date\", \"enumeration\" or \"boolean\": <pre>{$templateText}</pre>");
            if ($value->type == "enumeration" && !isset($value->values))
                throw new Exception("enumeration \"{$key}\" has no values property: <pre>{$templateText}</pre>");
            if ($value->type == "enumeration" && !is_array($value->values))
                throw new Exception("enumeration \"{$key}\" values property has to be an array: <pre>{$templateText}</pre>");
            if (isset($value->min) && !is_int($value->min) || isset($value->max) && !is_int($value->max))
                throw new Exception("\"{$key}\" property has a not integer min or max: <pre>{$templateText}</pre>");
            if (isset($value->min) && $value->min < 0 || isset($value->max) && $value->max < 0)
                throw new Exception("\"{$key}\" property has invalid min or max value: <pre>{$templateText}</pre>");
            if (isset($value->min) && isset($value->max) && $value->min > $value->max)
                throw new Exception("\"{$key}\" property has min greater than max: <pre>{$templateText}</pre>");
        }
    }

}