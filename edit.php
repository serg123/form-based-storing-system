<?php
include("helpers/methods.php");
include("model/TemplateManager.php");
include("model/DAO.php");
include("model/ArgumentParser.php");

try {
    if (isset($_POST["id"]))                 //lets treat <POST id=...> as an edit item submit request
        postEdit($_POST["id"], $_POST);
    else if (isset($_POST["template"]))     //lets treat <POST template=...> as a new item submit request
        postNew($_POST["template"], $_POST);
    else if (isset($_GET["id"]))             //lets treat <GET id=...> as an item edit form request
        getEdit($_GET["id"]);
    else if (isset($_GET["delete"]))         //lets treat <GET delete=...> as a delete by id request
        delete($_GET["delete"]);
    else if (isset($_GET["template"]))     //lets treat <GET template=...> as a new item form request
        getNew($_GET["template"]);
} catch (Exception $e) {
    render("error", ["message" => $e->getMessage()]);
}

function getEdit($id){
    $dao = new DAO();
    $templateFormData = $dao->getById($id);
    $templateManager = new TemplateManager();
    render("edit", ["templateManager" => $templateManager, "templateFormData" => $templateFormData]);
}

function getNew($template){
    $templateManager = new TemplateManager();
    render("new", ["templateName" => $template, "templateManager" => $templateManager]);
}

function delete($id){
    $dao = new DAO();
    $templateFormData = $dao->getById($id);
    $dao->deleteById($id);
    redirect("index.php", ["template" => $templateFormData["template"]]);
}

function postEdit($id, $formData){
    $dao = new DAO();
    $template = $dao->getById($id)["template"];
    $templateManager = new TemplateManager();
    $parser = new ArgumentParser($templateManager);
    $parsedFormData = $parser->parse($template, $formData);
    $dao->update($id, $parsedFormData);
    redirect("index.php", ["template" => $template]);
}

function postNew($template, $formData){
    $dao = new DAO();
    $templateManager = new TemplateManager();
    $parser = new ArgumentParser($templateManager);
    $parsedFormData = $parser->parse($template, $formData);
    $dao->create($template, $parsedFormData);
    redirect("index.php", ["template" => $template]);
}