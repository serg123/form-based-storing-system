<?php
function render($viewName, $args){
    foreach ($args as $key => $value)
        $$key = $value;
    include("view/" . $viewName . ".php");
}

function redirect($url, $args){
    $paramsWithValues = [];
    foreach ($args as $key => $value)
        $paramsWithValues[] = (urlencode($key) . "=" . urlencode($value));
    if (sizeof($paramsWithValues) > 0)
        $url .= ("?" . implode("&", $paramsWithValues));
    header('Location: ' . $url);
}

function control(&$value){
    return isset($value) ? $value : null;
}