CREATE DATABASE form_based_storing_system;
USE form_based_storing_system;
CREATE USER 'form_based_storing_system_user'@'localhost' IDENTIFIED BY 'form_based_storing_system_password';
GRANT ALL ON form_based_storing_system.* TO 'form_based_storing_system_user'@'localhost';
CREATE TABLE data_storage(
  id INT AUTO_INCREMENT PRIMARY KEY ,
  template VARCHAR(200) NOT NULL,
  data JSON NOT NULL,
  INDEX(template)
);